#ifndef __DATA_DUMPER_H__
#define __DATA_DUMPER_H__

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <Data/Window.hh>
#include <Globals.hh>

#include <Song.h>

#include <Utils/Logger.hh>

#undef DEFTAG
#define DEFTAG "DataDumper"

namespace playffle {

class DataDumper {
private:
  std::string m_name;
  std::string m_directory;
  playffle::Config_parser& params;

  void initPath(const std::string& song_path) {
    if(params.get_param<bool>("DebugOutput") ) {
      m_name = std::string(song_path.begin() + song_path.rfind("/"), song_path.end());
      m_directory = params.get_param<std::string>("TestOutDir");
    }
  }

public:
  DataDumper(const std::string& path) : params(playffle::Config_parser::instance()){
    initPath(path);
  }

  bool check(const std::string& input) {
		if(!params.get_param<bool>("DebugOutput"))
			return true;
		if(!params.get_param<bool>(input))
			return true;
		return false;
	}

  void dumpWindows(std::vector<Window>& data) {
    if(check("DumpWindowData"))
      return;

    std::ofstream file;
		file.open(m_directory +"windows/" + m_name + ".win", std::ios_base::trunc);

		uint32_t win_size = params.get_param<uint32_t>("WindowSize");
		LOGIPRINTF("Window size is: %d ", win_size);

    for(auto& window : data) {
			if(window.size() != win_size) LOGE("Window size error!!!");

      for(uint32_t j = 0; j < window.size(); ++j)
        file << window.data()[j] << " ";
      file << std::endl;
    }

    file.close();
    LOGIPRINTF("Windows data written for %s", m_name.c_str());
  }

  void dumpRaw(std::vector<Window>& data) {
		if(check("DumpRawData"))
      return;

    std::ofstream file;
		file.open(m_directory +"raw/" + m_name + ".raw", std::ios_base::trunc);

    for(auto& window : data) {
      for(uint32_t j = 0; j < window.size(); ++j)
        file << window.data()[j] << " ";
    }

    file.close();
    LOGIPRINTF("Raw data written for %s", m_name.c_str());
  }

  void dumpFFT(std::vector<Window>& data) {
		if(check("DumpRawData"))
      return;

    std::ofstream file;
		file.open(m_directory +"fft/" + m_name + ".fft", std::ios_base::trunc);

    uint32_t size = params.get_param<uint32_t>("WindowFFTBins");

    for(auto i = data.begin(); i != data.end(); ++i) {
      for(uint32_t j = 0; j < size; ++j)
        file << i->fftData()[j] << " ";
      file << std::endl;
    }

    file.close();

    LOGEPRINTF("Fft data written for %s", m_name.c_str());
  }

  void dumpFeatures(std::vector<std::vector<float>>& data) {
		if(check("DumpFeatureData"))
			return;

		std::ofstream file;
		file.open(m_directory +"features/" + m_name + ".ftrs", std::ios_base::trunc);

		for(auto window : data)
		{
			for(auto feature : window)
			{
				file << feature << " ";
			}
			file << std::endl;
		}

		file.close();

		LOGIPRINTF("Feature data written for %s", m_name.c_str());

	}

	void dumpBlocks(std::vector<std::vector<float>>& data) {
		if(check("DumpBlockData"))
			return;

		std::ofstream file;
		file.open(m_directory +"block/" + m_name + ".blck", std::ios_base::trunc);

		for(auto block : data)
		{
			for(auto feature : block)
			{
				file << feature << " ";
			}
			file << std::endl;
		}

		file.close();

		LOGIPRINTF("Block data written for %s", m_name.c_str());

	}

	void dumpWholeSetFeature(std::vector<float>& data) {
		if(check("DumpSingleData"))
			return;

		std::ofstream file;
		file.open(m_directory +"single/" + m_name + ".sngl", std::ios_base::trunc);

		uint16_t new_line = 0;
		for(auto dimension : data)
		{
			++new_line;
			file << dimension <<  " & ";
			if(new_line % 4 == 0){
				file << "\\\\" << std::endl;
			}
		}


		file << std::endl;
		file.close();

		LOGIPRINTF("Single block data written for %s", m_name.c_str());
	}

	void dumpStatsMaxMin(std::vector<Song>& songs){
		std::vector<float> min = songs[0].getFeatures();
		std::vector<float> max = songs[0].getFeatures();

		for(size_t i = 1; i < songs.size(); ++i) {
			auto& features = songs[i].getFeatures();
			for(size_t j = 0; j < features.size(); ++j)
			{
				if(min[j] > features[j])
					min[j] = features[j];

				if(max[j] < features[j])
					max[j] = features[j];
			}
		}

		float global_min = min[0];
		float global_max = max[0];

		for(auto minimum : min)
			if(global_min > minimum)
				global_min = minimum;

		for(auto maximum : max)
			if(global_max < maximum)
				global_max = maximum;

		std::ofstream file(m_directory + "stats_min_max.txt");
		for(size_t i = 0; i < min.size(); ++i){
			file << min[i] << " & " << max[i] << "\\\\" << std::endl;
		}

		file << std::endl << std::endl << std::endl ;
		file << "Global min: " << global_min << std::endl;
		file << "Global max: " << global_max << std::endl;

		file.close();
	}

};

} //end namespace playffle

#endif //__DATA_DUMPER_H__
