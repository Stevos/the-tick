#ifndef __FEATURE_GROUP_H__
#define __FEATURE_GROUP_H__

#include <Data/WindowFeatureExtractor.hh>

#include <vector>

using VVFloat = std::vector<std::vector<float>>;

namespace playffle {

class FeatureGroup {

public:
	FeatureGroup(const VVFloat& data);

	std::vector<float> getFeaturesForWholeSong();
	VVFloat getFeatureGroups();
	void setWindowPerBlock(uint32_t in);

private:
	const VVFloat& m_data;
	uint32_t m_win_per_group;

	void initParams();

	std::vector<float> getFeatureBlock(uint32_t start, uint32_t end);

	float f_mean		(uint32_t start,
									 uint32_t end,
								   WindowFeatureExtractor::WindowFeatures feature);

	float f_variance(uint32_t start,
									 uint32_t end,
									 WindowFeatureExtractor::WindowFeatures feature,
									 float mean);

	float f_skewness(uint32_t start,
									 uint32_t end,
									 WindowFeatureExtractor::WindowFeatures feature,
									 float mean,
									 float std_dev);

	float f_kurtosis(uint32_t start,
									 uint32_t end,
									 WindowFeatureExtractor::WindowFeatures feature,
									 float mean,
									 float std_dev);

};

} // end namespace playffle::


#endif // __FEATURE_GROUP_H__
