#include <Data/norm_window.h>

namespace playffle {

	NormalizationWindow::NormalizationWindow()
	{
		initFuncs();
	}

	std::unique_ptr<float[]> NormalizationWindow::get_window(const std::string& name) {
		auto& params = Config_parser::instance();

		uint32_t length = params.get_param<uint32_t>("WindowSize");
		uint32_t size = 0;
		std::unique_ptr<float[]> output(new float[length]);

		// If input name corresponds to format for file load
		// e.g. FILE@/home/stefan/test_win/hanning.txt
		if(simple_check(name)) {
			std::string path = std::string(name.begin() + 5, name.end());

			std::ifstream file;
			file.open(path);

			while(!file.eof() && length < size++)
				file >> output[length++];
		}

		if(m_windows.find(name) != m_windows.end()) {
			m_windows[name](output, (float)length);
		}
		else {
			if (length < size)
				throw RootException("NORM_WIN", "Different length of normalization window.");
			else
				throw RootException("NORM_WIN", "Normalization windows not found");
		}

		return output;
	}

	void NormalizationWindow::initFuncs() {
		m_windows["Bartlet"] = [](std::unique_ptr<float[]>& out, uint32_t N) {
					float L = N - 1;
					for(uint32_t i = 0; i < N; ++i) {
							out[i] =  1.f - ::abs( (i - (L/2)) / (L/2) );
					}
			};

			m_windows["Welch"] = [](std::unique_ptr<float[]>& out, uint32_t N) {
					float L = N - 1;
					for(uint32_t i = 0; i < N; ++i) {
							out[i] = 1 - ::pow( (i - (L/2)) / (L/2), 2.f);
					}
			};

			m_windows["sine_window"] = [](std::unique_ptr<float[]>& out, uint32_t N) {
					float L = N - 1;
					for(uint32_t i = 0; i < N; ++i) {
							out[i] = ::sin( M_PI / L * i);
					}
			};

			m_windows["Rectangular"] = [](std::unique_ptr<float[]>& out, uint32_t N) {
					for(uint32_t i = 0; i < N; ++i) {
							out[i] = 1.f;
					}
			};

			m_windows["Hann"] = [](std::unique_ptr<float[]>& out, uint32_t N) {
					float L = N - 1;
					for(uint32_t i = 0; i < N; ++i) {
							out[i] = ::pow(::sin( M_PI / L * i), 2);
					}
			};

			m_windows["Hamming"] = [](std::unique_ptr<float[]>& out, uint32_t N) {
					float L = N - 1;
					for(uint32_t i = 0; i < N; ++i) {
							out[i] = 0.54f - 0.46f * ::cos( 2 * M_PI / L * i);
					}
			};
	}

	bool NormalizationWindow::simple_check(const std::string& name) { // rather stupid than simple
		return name.at(0) == 'F' && name.at(1) == 'I' && name.at(2) == 'L' &&
			name.at(3) == 'E' && name.at(4) == '@';
	}

} // end of namespace playffle
