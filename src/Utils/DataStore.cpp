#include "Utils/DataStore.h"

#include "sqlite3.h"

DatastoreSqlite::DatastoreSqlite() {
	int rc;
	rc = sqlite3_open("local.db", &m_db);
	if(rc){
		throw DatastoreException("Fail to open DB", sqlite3_errmsg(m_db));
	}
}

DatastoreSqlite::~DatastoreSqlite() {
	sqlite3_close(m_db);
}

void DatastoreSqlite::insertFeatures(const std::string& path, const std::vector< float >& vec)
{
	std::string query = generateInputQuery(path, vec);
	this->executeInsertQuery(query);
}

bool DatastoreSqlite::checkRoutine()
{

	try {
		std::string query ("SELECT name FROM sqlite_master "
		"WHERE name = 'features'");

		int ret = 0;
		bool ret1 = checkIfThereIsResult(query, ret);

		if( ret1 != true && ret != 1){
			createTable();
		}

		return true;
	}
	catch(DatastoreException e){
		std::cout << "checkRoutine except: ";
		std::cout << e.what() << std::endl;
		return false;
	}
}

bool DatastoreSqlite::checkIfExist(const std::string& path)
{
	std::string query {"SELECT path FROM features WHERE path = '"};
	query += path;
	query += "'";

	int num = 0;
	bool ret = this->checkIfThereIsResult(query, num);
	return ret == true || num == 1;
}

std::vector< float > DatastoreSqlite::getFloat(const std::string& path)
{
	std::string query = "select features from features where path = '";
	query += path;
	query += "'";

	std::string ret = "";

	auto callback = [] (void* ret, int argc, char** argv, char** colName) -> int{
		std::string* ret1 = (std::string*) ret;
		*ret1 =  std::string(argv[0]);

		return 0;
	};

	char* err;
	int rc = sqlite3_exec(m_db, query.c_str(), callback, &ret, &err);

	if(rc) {
		throw DatastoreException("Get float! ", err);
	}

	return Parser::getFloat(ret);
}

void DatastoreSqlite::createTable()
{

	std::string query("CREATE TABLE features("
	" path TEXT	PRIMARY KEY NOT NULL,"
	" features TEXT NOT NULL"
	")");

	char* err;
	int rc = sqlite3_exec(m_db, query.c_str(), nullptr, nullptr, &err);

	if(rc){
		throw DatastoreException("CreateTable error!", err);
	}

}

bool DatastoreSqlite::checkIfThereIsResult(const std::string& query, int& num)
{
	sqlite3_stmt *stmt;
	bool ret = false;
	num = -1;

	int rc = sqlite3_prepare_v2(m_db, query.c_str(), -1, &stmt, NULL);

	if (rc != SQLITE_OK) {
		throw DatastoreException("Check error!: ", sqlite3_errmsg(m_db));
	}

	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
		DatastoreException thr("Check error!: ", sqlite3_errmsg(m_db));
		sqlite3_finalize(stmt);

		throw thr;
	}

	if (rc == SQLITE_DONE)
		return false;
	else if (sqlite3_column_type(stmt, 0) == SQLITE_NULL)
		return false;
	else {
		num = sqlite3_column_int(stmt, 0);
		ret = true;
	}

	sqlite3_finalize(stmt);

	return ret;
}

std::string DatastoreSqlite::generateInputQuery(const std::string& path, const std::vector< float >& vec)
{
	std::string ret {"INSERT INTO features (path, features) VALUES ('"};
	ret += path;
	ret += "','";
	ret += Parser::getString(vec);
	ret += "')";

	return ret;
}

void DatastoreSqlite::executeInsertQuery(const std::string& query)
{
	int rc = sqlite3_exec(m_db, query.c_str(), nullptr, 0, nullptr);

	if(rc != SQLITE_OK){
		throw DatastoreException("ExecuteInQuery Error! ", sqlite3_errmsg(m_db));
	}
}
