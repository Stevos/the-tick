#ifndef __PARSER_H__
#define __PARSER_H__

#include <functional>
#include <iterator>
#include <string>
#include <sstream>
#include <vector>

class Parser {
public:
	static std::string getString(const std::vector<float>& vec){
		std::string ret;

		for(float item : vec) {
			ret += std::to_string(item);
			ret += "%";
		}

		return ret;
	}

	static std::vector<float> getFloat(const std::string& str, bool def = true) {
		float temp;
		char c;
		int i = 0;
		std::vector<float> ret(def ? 40 : 0, 0.f);
		std::function<void(float&)> op;

		if(def){
			op = [&ret,&i](float& temp){ ret[i] = temp; ++i; };
		}
		else{
			op = [&ret](float& temp) {ret.push_back(temp);};
		}

		std::stringstream ss(str);

		while(ss >> temp){
			ss >> c;
			op(temp);
		}
		return ret;
	}

};

#endif //__PARSER_H__
