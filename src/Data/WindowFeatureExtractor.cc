//
// Created by stefan on 23.6.16..
//

/// @note Feature calculation can be seriously improved by merging a lot of features into
/// one for loop to avoid unnecessary looping through same data. This is for testing and
/// easier maintaining until performance is required and tests are done.

#include <Globals.hh>

#include "Data/WindowFeatureExtractor.hh"
#include "Utils/Logger.hh"
#include "Data/StasticalData.hh"
#include <cmath>
#include <fstream>

#undef DEFTAG
#define DEFTAG "WindowFeatureExtractor"

using playffle::Config_parser;

WindowFeatureExtractor::WindowFeatureExtractor(WindowData* data)
: m_data(data),
  m_Sx(0),
  m_Sxx(0),
  m_total_amp(0.f)
{
  initParams();
	initValues();
}

WindowFeatureExtractor::~WindowFeatureExtractor()
{
}


void WindowFeatureExtractor::initParams()
{
	m_win_size = Config_parser::instance().get_param<uint32_t>("WindowSize");
	m_dest_samplerate = Config_parser::instance().get_param<uint32_t>("DestinationSampleRate");
	m_win_per_sec = Config_parser::instance().get_param<uint32_t>("WindowsPerSecond");
	m_fft_bins = Config_parser::instance().get_param<uint32_t>("WindowFFTBins");

	LOGDPRINTF("Number of bins: %d", m_fft_bins);
	LOGDPRINTF("WindowSize: %d", m_win_size);
}

/// @pre Spectral Centroid method must be called before this one to initialize max amp.
///
float WindowFeatureExtractor::f_linearRegression_fftf(float* data)
{
	float n = m_fft_bins - 1;
	float Sxy = 0, Sy = m_total_amp;

	unsigned int freq = 0;

	for(unsigned int i = 1; i < m_fft_bins; ++i){
		freq = i * m_win_per_sec;
		Sxy += freq * data[i];
	}
	// Minimal square fit
	return ((n * Sxy) - (m_Sx * Sy)) / ((n * m_Sxx) - (m_Sx * m_Sx));
	float total = 0;
	float bins = 0;

	for(unsigned int i = 1; i < m_fft_bins; ++i){
		bins += i * m_win_per_sec * data[i];
		total += data[i];
	}

	m_total_amp = total; // Used for further features
	return  bins / total;
}

float WindowFeatureExtractor::f_spectralCentroid_fftf(float* data)
{
	float total = 0;
	float bins = 0;

	for(unsigned int i = 1; i < m_fft_bins; ++i){
		bins += i * m_win_per_sec * data[i];
		total += data[i];
	}

	m_total_amp = total; // Used for further features
	return  bins / total;
}

float WindowFeatureExtractor::f_spectralSmoothness_fftf(float* data)
{
	float spectralSmooth = 0;
	for(uint32_t i = m_fft_bins -1; i != 2; --i){
		spectralSmooth += (20 * logf(data[i])) - ((
			(20 * logf(data[i-1])) +
			(20 * logf(data[i])) +
			(20 * logf(data[i+1]))
		) / 3);
	}
	return fabsf(spectralSmooth);
}

/// @pre Spectral Centroid method must be called before this one to initialize max amp.
float WindowFeatureExtractor::f_spectralDissymetry_fftf(float* data, float spectral_centroid)
{
	float dissym = 0;
	for(uint32_t i = 0; i < m_fft_bins; ++i){
		dissym += data[i] * powf( ( ( (float) i / m_fft_bins) * (m_dest_samplerate/2)) -  spectral_centroid, 3);
	}
	return cbrtf(dissym / m_total_amp);

}

/// @pre Spectral Centroid method must be called before this one to initialize max amp.
float WindowFeatureExtractor::f_spectralSpread_fftf(float* data, float spectral_centroid)
{
	float spread = 0;
	for(uint32_t i = 0; i < m_fft_bins; ++i){
		spread += data[i] * powf( ( ( (float) i / m_fft_bins) * (m_dest_samplerate/2)) - spectral_centroid ,2);
	}
	return sqrtf(spread / m_total_amp);
}

void WindowFeatureExtractor::initValues() {
  m_Sx = 0; m_Sxx = 0;
  for(uint32_t i = 1; i < m_fft_bins; ++i){
    m_Sx += i * m_win_per_sec;
    m_Sxx += (i * m_win_per_sec) * (i * m_win_per_sec);
  }
}

std::vector<float> WindowFeatureExtractor::windowFeatureCalc(Window& win)
{
	std::vector<float> ret (WindowFeatures::WINDOW_FEATURE_SIZE);

	ret[ZERO_CROSSING_RATE] = f_zeroCrossingRate(win.data());
	ret[FIRST_ORDER_AUTOCORRELATION] = f_firstOrderCorrelation(win.data());
	ret[RMS_ENERGY] = f_rms_energy(win.data());
	ret[MAX_AMPLITUDE] = f_max_amp(win.data());
	ret[SPECTRAL_CENTROID] = Globals::checkValue(f_spectralCentroid_fftf(win.fftData()));
	ret[LINEAR_REGRESSION] = Globals::checkValue(f_linearRegression_fftf(win.fftData()));
	ret[SPECTRAL_SMOOTHNESS] = Globals::checkValue(f_spectralSmoothness_fftf(win.fftData()));
	ret[SPECTRAL_DISSYMETRY] = Globals::checkValue(f_spectralDissymetry_fftf(win.fftData(), ret[SPECTRAL_CENTROID]));
	ret[SPECTRAL_SPREAD] = Globals::checkValue(f_spectralSpread_fftf(win.fftData(), ret[SPECTRAL_CENTROID]));

	return ret;
}

VVFloat WindowFeatureExtractor::getFeatures()
{
	std::vector<Window>& data = m_data->getData();
	uint32_t size = data.size();

	VVFloat ret(size);

	for(uint32_t i = 0; i < size; ++i)
	{
		ret[i] = windowFeatureCalc(data[i]);
	}

	normalize(ret);

	return ret;
}

void WindowFeatureExtractor::normalize(VVFloat& data)
{
	std::vector<float> min (WINDOW_FEATURE_SIZE, 0.f);
	std::vector<float> max (WINDOW_FEATURE_SIZE, 0.f);

	// get border values
	for(auto& features : data) {
		for(uint16_t i = 0; i < WINDOW_FEATURE_SIZE; ++i)
		{
			if(std::isnan(features[i]) || std::isinf(features[i])) continue;

			if(features[i] > max[i]) {
				max[i] = features[i];
			}

			if(features[i] < min[i]) {
					min[i] = features[i];
			}
		}
	}

	std::ofstream file("/home/stefan/test_out/max_min.txt");

	for(size_t i = 0; i < min.size(); ++i)
		file << min[i] << " " << max[i] << std::endl;

	for(auto& features : data) {
		for(uint16_t i = 0; i < WINDOW_FEATURE_SIZE; ++i) {
			features[i] = Globals::checkValue(-1 + (features[i] - min[i]) / (max[i] - min[i]) * 2);
			//features[i] = Globals::checkValue(features[i]);
		}
	}
}

