#ifndef __NORMALIZATION_WINDOW__H__
#define __NORMALIZATION_WINDOW__H__

#include <array>
#include <iostream>
#include <fstream>
#include <functional>
#include <string>
#include <memory>
#include <unordered_map>

#include <cmath>

#include <Utils/RootException.hh>

#include <Globals.hh>

namespace playffle {

  /*
   * API for loading normalization window to normalize data for FFT, to avoid
   * spectral leaking as sideeffect of using portion of sample for processing
   * a.k.a. Windows.
   *
   * Provide ability to generate normalization windows ad-hoc or load from file
   *
   */

  class NormalizationWindow {
      using WinFunc = std::function< void(std::unique_ptr<float[]>&, uint32_t)>;
  public:
    NormalizationWindow();

    std::unique_ptr<float[]> get_window(const std::string& name);
  private:

    std::unordered_map<std::string, WinFunc> m_windows;

    void initFuncs();
    bool simple_check(const std::string& name);

  };

}

#endif // __NORMALIZATION_WINDOW__H__
