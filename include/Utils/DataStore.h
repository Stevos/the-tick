#ifndef __DATASTORE_H__
#define __DATASTORE_H__

#include "sqlite3.h"

#include "Parser.h"
#include "RootException.hh"

#include <iostream>


#include <string>
#include <vector>

class IDataStore {

public:
	/* Check if datastore is properly setup and ready to use.
	 * NOTE: This can possibly be moved into constructor
	 */
	virtual bool checkRoutine() = 0;

	/*
	 * Insert caluclated features into datastore
	 */
	virtual void insertFeatures(const std::string& path, const std::vector<float>& vec) = 0;
};

class DatastoreSqlite : public IDataStore {
public:
	DatastoreSqlite();

	~DatastoreSqlite();

	void insertFeatures(const std::string& path, const std::vector<float>& vec);

	bool checkRoutine();

	bool checkIfExist(const std::string& path);

	std::vector<float> getFloat(const std::string& path);

private:
	sqlite3* m_db;

	bool 				checkIfThereIsResult(const std::string& query, int& num);
	void 				createTable();
	void 				executeInsertQuery(const std::string& query);
	std::string generateInputQuery(const std::string& path,
																 const std::vector<float>& vec);

};



#endif //__DATASTORE_H__
