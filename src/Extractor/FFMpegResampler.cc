#include "Extractor/FFMpegResample.hh"

#undef DEFTAG
#define DEFTAG "FFMpegResampler"

// Constructor
FFMpegResample::FFMpegResample()
: m_dst_ch_layout (AV_CH_LAYOUT_MONO), m_dst_smpl_fmt(AV_SAMPLE_FMT_FLT),
m_dst_rate (playffle::Config_parser::instance().get_param<uint32_t>("DestinationSampleRate")),
m_dst_nb_samples(0),
m_output(nullptr),
m_swr_ctx(nullptr)
{}

FFMpegResample::~FFMpegResample(){
	// Deallocate m_output
	if(m_output != nullptr)
	  av_free(m_output);
	if(m_swr_ctx != nullptr)
	  swr_free(&m_swr_ctx);
}

/*
 * Initializing resampler on target output values.
 * NOTE: Input sample rate is sum of all channels combined.
 * */

bool FFMpegResample::initResampler(
	enum AVSampleFormat src_smpl_fmt, int64_t src_ch_layout,
	int src_rate, int src_nb_samples ){

	// Source information initalization
	m_src_smpl_fmt = src_smpl_fmt;
	m_src_ch_layout = src_ch_layout;
	m_src_rate = src_rate;
	m_src_nb_samples = src_nb_samples;

	m_swr_ctx = swr_alloc();
	if (!m_swr_ctx)
		throw RootException(DEFTAG, "Could not allocate resampler context!");

	/* set options */
	av_opt_set_int(m_swr_ctx, "in_channel_layout",    m_src_ch_layout, 0);
	av_opt_set_int(m_swr_ctx, "in_sample_rate",       m_src_rate, 0);
	av_opt_set_sample_fmt(m_swr_ctx, "in_sample_fmt", m_src_smpl_fmt, 0);
	av_opt_set_int(m_swr_ctx, "out_channel_layout",    m_dst_ch_layout, 0);
	av_opt_set_int(m_swr_ctx, "out_sample_rate",       m_dst_rate, 0);
	av_opt_set_sample_fmt(m_swr_ctx, "out_sample_fmt", m_dst_smpl_fmt, 0);

	// Initialize swr
	if (swr_init(m_swr_ctx) < 0)
		throw RootException(DEFTAG, "Failed to initialize the resampling context");


	m_src_nb_channels = av_get_channel_layout_nb_channels(m_src_ch_layout);

	LOGD("Resampler initialized!");
	return true;
}

bool FFMpegResample::resampleAudioFrame(AVFrame* frame, WindowData* data){

	// Compute number of output samples.
	int nb_of_samples = av_rescale_rnd(swr_get_delay(m_swr_ctx, frame->sample_rate) +
	frame->nb_samples, m_dst_rate, frame->sample_rate, AV_ROUND_UP);

	// Allocate space
	if(nb_of_samples > m_dst_nb_samples){
		m_dst_nb_samples = nb_of_samples;
		av_free(m_output);
		av_samples_alloc(&m_output, NULL, 1, m_dst_nb_samples,
				 m_dst_smpl_fmt, 0);
	}

	nb_of_samples = swr_convert(m_swr_ctx, &m_output, nb_of_samples,
				  // Fucking const in diamonds!
				  const_cast<const uint8_t**>(frame->data), frame->nb_samples);

	// This is affecting number of output samples.
	processOutputBuffer(data, nb_of_samples);

	return true;
}

// IMPROVEMENT: Data normalization

void FFMpegResample::processOutputBuffer(WindowData* data, int size_of_buffer){
	int16_t dest;
	for(int i = 0; i < size_of_buffer; i += 2){
		memcpy(&dest, m_output+i, sizeof(int16_t));
		//destfl = dest * 1.f / 255.f; //< Normalization
		data->addSample((float)dest);
	}
// 	std::cout << "***********************************************" << std::endl;
// 	std::cout << "* MaxBoundSample is " << max << " * " << std::endl;
// 	std::cout << "* MaxBoundSample is " << min << " * " << std::endl;
// 	std::cout << "***********************************************" << std::endl;
}
