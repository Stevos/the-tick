#ifndef __NEWHANDLE__H__
#define __NEWHANDLE__H__

#include "Globals.hh"
#include "FileSystem/FileHandle.hh"

#include <experimental/filesystem>
#include <list>
#include <string>

namespace fs = std::experimental::filesystem;

class FolderHandle {
public:
	FolderHandle(std::string path, short formats = 1)
		:m_iterated(false), m_path(path), m_formats(formats){}

	~FolderHandle() {}

	std::list<FileHandle>& getFiles(){
		if(!m_iterated)
			generateFileList(m_path);

		return m_list;
	}

private:
	bool m_iterated;
	std::string m_path;
	short m_formats;

	std::list<FileHandle> m_list;


	void generateFileList(const std::string& path){
		if(!m_iterated) {
			for(const auto& p: fs::recursive_directory_iterator(path)){
				if(
					(p.path().extension() == ".mp3" && ((m_formats & (int)Globals::FileFormats::MP3) != 0)) ||
					(p.path().extension() == ".wmv" && ((m_formats & (int)Globals::FileFormats::WMV) != 0)) ||
					(p.path().extension() == ".aac" && ((m_formats & (int)Globals::FileFormats::AAC) != 0)) ||
					(p.path().extension() == ".ogg" && ((m_formats & (int)Globals::FileFormats::OGG) != 0)) ||
					(p.path().extension() == "flac" && ((m_formats & (int)Globals::FileFormats::FLAC) != 0))
				) {
					FileHandle f(p);
					m_list.push_back(f);
				}
			}
		}

		m_iterated = true;
	}

};

#endif
