//
// Created by stefan on 23.6.16..
//

#ifndef FFMPEG_FEATUREEXTRACTOR_HH
#define FFMPEG_FEATUREEXTRACTOR_HH

#include "Data/StasticalData.hh"
#include "Data/Window.hh"
#include "Data/WindowData.hh"
#include "Globals.hh"
#include <Data/norm_window.h>

#include <functional>
#include <string>
#include <vector>
#include <cmath>

using VVFloat = std::vector<std::vector<float>>;

/*
 * Class WindowFeatureExtractor
 *
 * Responsible for extracting stats and features from window data.
 */

class WindowFeatureExtractor {
public:
	/// Enumeration for extracted window features
	enum WindowFeatures {
		ZERO_CROSSING_RATE = 0,
		FIRST_ORDER_AUTOCORRELATION,
		RMS_ENERGY,
		MAX_AMPLITUDE,
		SPECTRAL_CENTROID,
		LINEAR_REGRESSION,
		SPECTRAL_SMOOTHNESS,
		SPECTRAL_SPREAD,
		SPECTRAL_DISSYMETRY,
		WINDOW_FEATURE_SIZE // Must be last in this enum.
	};

	WindowFeatureExtractor(WindowData* data);
	~WindowFeatureExtractor();

	VVFloat getFeatures();

private:

	const float m_ber_value = 2000.f;

	WindowData* m_data;

  // This will create small overhead, but not so much.
  uint32_t m_win_size;
  uint32_t m_win_per_sec;
  uint32_t m_fft_bins;
  uint32_t m_dest_samplerate;

  int m_Sx, m_Sxx;
	float m_total_amp;

	void initNormalizationWindow();
	void initParams();
	void initValues();

	std::vector<float> windowFeatureCalc(Window& win);

	void normalize(VVFloat& data);

	// ********************************************************
	// **            Feature calculation methods             **
	// ********************************************************

	template<typename T>
	float f_zeroCrossingRate(T* data);

	template<typename T>
	float f_firstOrderCorrelation(T* data);

  template<typename T>
  float f_rms_energy(T* data);

	template<typename T>
	float f_max_amp(T* data);

	float f_spectralCentroid_fftf(float* data);

  float f_linearRegression_fftf(float* data);

  float f_spectralSmoothness_fftf(float* data);

	float f_bandEnergyRatio_fftf(float* data);

  float f_spectralDissymetry_fftf(float* data, float spectral_centroid);

	float f_spectralSpread_fftf(float* data, float spectral_centroid);

};

template <typename T>
float WindowFeatureExtractor::f_zeroCrossingRate(T* data)
{
  float zero_crossing_rate = 0.f;
  for(uint16_t i = 1; i < m_win_size; ++i)
  {
    zero_crossing_rate += (data[i-1] * data[i]) < 0;
  }

  return zero_crossing_rate * 1.f / m_win_size;
}

template <typename T>
float WindowFeatureExtractor::f_firstOrderCorrelation(T* data)
{
  float first_order_autocorrelation = 0;

  for(uint16_t i = 1; i < m_win_size; ++i){
    first_order_autocorrelation += data[i-1] * data[i];
  }

  return first_order_autocorrelation / m_win_size;
}

template <typename T>
float WindowFeatureExtractor::f_rms_energy(T* data)
{
	float energy = 0;

	for(uint16_t i = 0; i < m_win_size; ++i)
	{
		energy += powf(data[i], 2.0f);
	}

	return ::sqrt(energy / m_win_size);
}

template <typename T>
float WindowFeatureExtractor::f_max_amp(T* data)
{
	float max = 0.f;
	for(uint16_t i = 0; i < m_win_size; ++i)
	{
		if(data[i] > max)
			max = data[i];
	}
	return max;
}


#endif //FFMPEG_FEATUREEXTRACTOR_HH
