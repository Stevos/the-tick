#ifndef __NOLOG_MACROS__H__
#define __NOLOG_MACROS__H__


/*
 * Using helper macros to be able to log file, function and line from where log is triggered.
 */

#undef FLOG
#define FLOG(Level, Tag, Msg) 
#undef LOG
#define LOG(Level, Tag, Msg) 

// With predefined tag

#undef LOGI
#define LOGI(Msg) 
#undef LOGW
#define LOGW(Msg) 
#undef LOGE
#define LOGE(Msg) 
#undef LOGD
#define LOGD(Msg) 

// Log macros with custom tags.

#undef ALOGI
#define ALOGI(Tag, Msg) 
#undef ALOGW
#define ALOGW(Tag, Msg) 
#undef ALOGE
#define ALOGE(Tag, Msg) 
#undef ALOGD
#define ALOGD(Tag, Msg) 

/*
 * Printf logging with predefined tag.
 */

#undef LOGIPRINTF
#define LOGIPRINTF(Msg, ...) 

#undef LOGWPRINTF
#define LOGWPRINTF(Msg, ...) 

#undef LOGEPRINTF
#define LOGEPRINTF(Msg, ...) 

#undef LOGDPRINTF
#define LOGDPRINTF(Msg, ...) 

/*
 * Printf logging with custom tag.
 */

#undef ALOGIPRINTF
#define ALOGIPRINTF(Tag, Msg, ...) 

#undef ALOGWPRINTF
#define ALOGWPRINTF(Tag, Msg, ...) 

#undef ALOGEPRINTF
#define ALOGEPRINTF(Tag, Msg, ...) 

#undef ALOGDPRINTF
#define ALOGDPRINTF(Tag, Msg, ...) 

#endif // __LOG_MACROS__H__