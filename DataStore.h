#ifndef __DATASTORE_H__
#define __DATASTORE_H__

#include "sqlite3.h"

#include "Parser.h"
#include "RootException.hh"

#include <iostream>


#include <string>
#include <vector>

class DatastoreSqlite {
public:
	DatastoreSqlite() {
		int rc;
		rc = sqlite3_open("local.db", &m_db);
		if(rc){
			throw DatastoreException("Fail to open DB", sqlite3_errmsg(m_db));
		}
	}

	~DatastoreSqlite() {
		sqlite3_close(m_db);
	}

	bool insertFeatures(const std::string& path, const std::vector<float>& vec){
		std::string parsed_vec = Parser::getString(vec);
		return false;
	}

	void createTable(){

		std::string query("CREATE TABLE features("
											" path TEXT	PRIMARY KEY NOT NULL,"
											" features TEXT NOT NULL"
											")");

		char* err;
		int rc = sqlite3_exec(m_db, query.c_str(), nullptr, nullptr, &err);

		if(rc){
			throw DatastoreException("CreateTable error!", err);
		}

	}

	bool checkRoutine(){
		std::string query ("SELECT name FROM sqlite_master"
												"WHERE name='features'");

		int rc;
		char* err;

		rc = sqlite3_exec(m_db, query.c_str(), );

	}


private:
	sqlite3* m_db;

};



#endif //__DATASTORE_H__
