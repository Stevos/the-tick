#ifndef __FFMPEGRESAMPLE__HH__
#define __FFMPEGRESAMPLE__HH__ 1


// IMPROVEMENT: Audio filtering as improved feature.
// Research possibility of adding various audio filters to improve results of latter analysis.


extern "C" {
	#include "libavcodec/avcodec.h"
	#include "libavdevice/avdevice.h"
	#include "libavfilter/avfilter.h"
	#include "libavformat/avformat.h"
	#include "libavutil/avutil.h"
	#include "libswresample/swresample.h"
}

///////////////////////////////////////////////////////////////////////////////

#include "Utils/Logger.hh"
#include "Data/WindowData.hh"
#include "Globals.hh"
#include "Utils/RootException.hh"

class FFMpegResample {
	
	enum AVSampleFormat m_src_smpl_fmt;
	int64_t m_src_ch_layout;
	int m_src_rate;
	int m_src_nb_samples;
	int m_src_nb_channels;
	
	int64_t m_dst_ch_layout;
	enum AVSampleFormat m_dst_smpl_fmt;
	int m_dst_rate;
	int m_dst_nb_samples;	
	
	uint8_t* m_output;
	
	struct SwrContext *m_swr_ctx;
	
	void processOutputBuffer(WindowData* data, int size_of_buffer);
	
public:
	FFMpegResample();
	~FFMpegResample();
	
	bool initResampler(
		enum AVSampleFormat src_smpl_fmt, int64_t src_ch_layout, 
		int src_rate, int src_nb_samples );
	
	bool resampleAudioFrame(AVFrame* frame, WindowData* data);
};

#endif //__FFMPEGRESAMPLE__HH__