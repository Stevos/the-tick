#ifndef __ROOTEXCEPTION__HH__
#define __ROOTEXCEPTION__HH__

#include <exception>
#include <string>
#include "Utils/LogMacros.hh"
#include "Utils/Logger.hh"

class RootException : public std::exception {
  std::string m_msg;
public:
  RootException(const std::string& tag, const std::string& msg) : m_msg(msg){
    ALOGE(tag.c_str(), msg.c_str());
  }
  virtual const char* what() { return m_msg.c_str(); }
};

class DistanceException : public std::exception{
	std::string m_msg;
public:
	DistanceException(const std::string& msg) : m_msg(msg) {}
	virtual const char* what() { return m_msg.c_str(); }
};

class DatastoreException : public std::exception {
	std::string m_msg;
public:
	DatastoreException(const std::string& msg, const std::string& sql3err) : m_msg(msg + " : " + sql3err) {}
	virtual const char* what() { return m_msg.c_str(); }
};

#endif //__ROOTEXCEPTION__HH__
