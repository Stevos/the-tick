
// Created by stefan on 18.12.16..
//

#ifndef FFMPEG_OCTAVEDATASAMPLER_H
#define FFMPEG_OCTAVEDATASAMPLER_H

#include <string>
#include <list>

#include "include/FileSystem/FileHandle.hh"
#include "include/FileSystem/FolderHandleLinux.hh"
#include "Globals.hh"
#include "RootComponent.hh"

 #include <sys/types.h>
 #include <sys/stat.h>
 #include <fcntl.h>

class OctaveDataSampler {
  
inline bool fileExists (const std::string& name) {
  struct stat buffer;   
  std::string temp = name.substr(name.rfind("/")+1);
  temp = std::string("/home/stefan/stats/") + temp + std::string(".stat");
  return (stat (temp.c_str(), &buffer) == 0); 
}
  
public:
    OctaveDataSampler(){}
    void proccessFolder(const std::string& path){
      // 63 as 11111 binary to cover all file formats flags
      FolderHandleLinux folder_handle(path, (int)Globals::FileFormats::MP3);
      folder_handle.setDepth(50);
      folder_handle.getFileList();
      std::list<FileHandle>& all_files = folder_handle.getList();
      std::cout << all_files.size() << std::endl;
      RootComponent root;
			std::cout << all_files.front().getName() << std::endl;
			root.processOne(all_files.front());
			root.storeCalculatedStats();
      
		}
};

#endif //FFMPEG_OCTAVEDATASAMPLER_H
