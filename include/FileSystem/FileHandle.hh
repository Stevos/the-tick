#ifndef __FILE_HANDLE__H__
#define __FILE_HANDLE__H__

/*
 * Include for FileHandle.
 *
 *
 * Intended to interact with physical representation of files on file system.
 * Most important feature is to implement various tag handling. Format of metadata
 * which come with audio song is format dependent. From ID3v1 and ID3v2 to OGG APE
 * tag formats.
 *
 * To be easily transported around with file data should be stored as minimalistic as
 * possible not to affect greatly on file size. Also should be carefully imported
 * inside metadata following rules which metadata is in first place created.
 *
 * Inside of ID3v2 tags, frame in which we will store data will be 'TPFL'.
 *
 * TODO: Need to be tested how many information can, should and need to be stored.
 *
 *
 * TODO:As data which is stored inside of tags is majorly stored as text value, should be
 * created parser for data.
 *
 */



#include <experimental/filesystem>
#include <fstream>
#include <string>
#include <vector>

#include "Utils/Logger.hh"
#include "Globals.hh"

namespace fs = std::experimental::filesystem;

class FileHandle {
public:
	FileHandle(const fs::directory_entry& entry);
	FileHandle(const std::string& path);
	
	inline std::string getPath() { return m_path.string(); }
	inline std::string getName() { return m_path.filename(); }

	std::string toString();

	~FileHandle();
private:
	fs::path m_path;
	Globals::FileFormats m_ext;

	Globals::FileFormats getFormat(const fs::path& path);
};

#endif // __FILE_HANDLE__H__
