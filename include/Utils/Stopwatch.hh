//
// Created by stefan on 19.6.16..
//

#ifndef FFMPEG_STOPWATCH_HH
#define FFMPEG_STOPWATCH_HH

#include <chrono>

class Stopwatch{

  std::chrono::high_resolution_clock::time_point m_start;
  std::chrono::high_resolution_clock::time_point m_end;
public:
  Stopwatch(){}

  void start(){
    m_start = std::chrono::high_resolution_clock::now();
  }

  void end(){
    m_end = std::chrono::high_resolution_clock::now();
  }

  int elapsed(){
    return std::chrono::duration_cast<std::chrono::milliseconds>(m_end-m_start).count();
  }
};

#endif //FFMPEG_STOPWATCH_HH
