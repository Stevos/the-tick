#include "Playlist.h"

#include <vector>

#include "data_dumper.h"

#include "Globals.hh"
#include <config_parser.h>

namespace {
	auto& params = playffle::Config_parser::instance();
}

using VVFloat = std::vector<std::vector<float>>;

Playlist::~Playlist()
{
}

void Playlist::addFolder(const std::string& path,
												 int depth,
												 Globals::FileFormats formats)
{
	FolderHandle folder_handle(path, (int)formats);
  std::list<FileHandle>& all_files = folder_handle.getFiles();

	for(FileHandle& file : all_files){
		this->addSong(file);
	}

}

void Playlist::addSong(const std::string& path)
{
	Song song(path);
	this->processOne(song);
	m_songList.push_back(song);
}

void Playlist::addSong(FileHandle& file)
{
	Song song(file);
	this->processOne(song);
	m_songList.push_back(song);
}

void Playlist::addRefSong(const std::string& path)
{
	m_ref = Song(path);
	processOne(m_ref);
	m_refCheck = true;
}


void Playlist::processOne(Song& song)
{
	Stopwatch clock;
	try {

		clock.start();
		RawDataExtractor extractor(song.path());
		std::cout << "Path: " << song.path() << std::endl;

    playffle::DataDumper dumper(song.path());

		if(!m_datastore.checkIfExist(song.path())){
			WindowData* data = extractor.extractData();

			data->introduceHopping();

			data->validate();
			data->FFTWindows();

			uint32_t num_of_frames = data->getData().size();
			(void) num_of_frames;

      dumper.dumpRaw(data->getData());
      dumper.dumpWindows(data->getData());
			dumper.dumpFFT(data->getData());

			WindowFeatureExtractor featureExtract(data);

			VVFloat features = featureExtract.getFeatures();
			dumper.dumpFeatures(features);

			playffle::FeatureGroup grouping(features);

			// Get feature calculation other out for now
// 			if(params.get_param<bool>("DumpBlockData")) {
// 				VVFloat feature_block = grouping.getFeatureGroups();
// 				dumper.dumpBlocks(feature_block);
// 			}

			std::vector<float> single_block_features = grouping.getFeaturesForWholeSong();
			song.setFeatures(single_block_features);


			// Delete raw data as we dont need that shit anymore
			delete data;
		}
		else{
			std::vector<float> vec = m_datastore.getFloat(song.path());
			song.setFeatures(vec);
		}

		extractor.fillMetaData(song);


		LOGIPRINTF("Time needed to process one song is %d ms ! <<<<<<<<<<<<", clock.elapsed());
	}
	catch(RootException e){
		std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
		std::cout << "<< " << song.path() << std::endl;
		std::cout << "<< " << e.what() << std::endl;
		std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
	}
	catch(DatastoreException e){
		std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
		std::cout << "<< " << song.path() << std::endl;
		std::cout << "<< " << e.what() << std::endl;
		std::cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
	}

	clock.end();
}


