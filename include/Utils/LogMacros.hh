#ifndef __LOG_MACROS__H__
#define __LOG_MACROS__H__


/*
 * Using helper macros to be able to log file, function and line from where log is triggered.
 */

#define FLOG(Level, Tag, Msg) {Logger& l = Logger::getLogger(); l.log(Level,Tag, __FUNCTION__, __FILE__, __LINE__, Msg); }
#define LOG(Level, Msg) {Logger& l = Logger::getLogger(); l.log(Level, DEFTAG, __FUNCTION__, __FILE__, __LINE__, Msg); }

// With predefined tag

#define LOGI(Msg) {Logger::getLogger().log(LoggerLevel::INFO, DEFTAG, __FUNCTION__, __FILE__, __LINE__, Msg); }
#define LOGW(Msg) {Logger::getLogger().log(LoggerLevel::WARNING, DEFTAG, __FUNCTION__, __FILE__, __LINE__, Msg);} 
#define LOGE(Msg) {Logger::getLogger().log(LoggerLevel::ERROR, DEFTAG, __FUNCTION__, __FILE__, __LINE__, Msg); }
#define LOGD(Msg) {Logger::getLogger().log(LoggerLevel::DEBUG, DEFTAG, __FUNCTION__, __FILE__, __LINE__, Msg); }

// Log macros with custom tags.

#define ALOGI(Tag, Msg) {(Logger::getLogger()).log(LoggerLevel::INFO, Tag, __FUNCTION__, __FILE__, __LINE__, Msg);}
#define ALOGW(Tag, Msg) {Logger::getLogger().log(LoggerLevel::WARNING, Tag, __FUNCTION__, __FILE__, __LINE__, Msg);}
#define ALOGE(Tag, Msg) {Logger::getLogger().log(LoggerLevel::ERROR, Tag, __FUNCTION__, __FILE__, __LINE__, Msg);}
#define ALOGD(Tag, Msg) {Logger::getLogger().log(LoggerLevel::DEBUG, Tag, __FUNCTION__, __FILE__, __LINE__, Msg);} 

/*
 * Printf logging with predefined tag.
 */

#define LOGIPRINTF(Msg, ...) { \
char msgBuff[256];  \
sprintf(msgBuff, Msg, __VA_ARGS__); \
Logger::getLogger().log(LoggerLevel::INFO, DEFTAG, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \

#define LOGWPRINTF(Msg, ...) { \
char msgBuff[256];  \
sprintf(msgBuff, Msg, __VA_ARGS__); \
Logger::getLogger().log(LoggerLevel::WARNING, DEFTAG, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \

#define LOGEPRINTF(Msg, ...) { \
char msgBuff[256];  \
sprintf(msgBuff, Msg, __VA_ARGS__); \
Logger::getLogger().log(LoggerLevel::ERROR, DEFTAG, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \

#define LOGDPRINTF(Msg, ...) { \
char msgBuff[256];  \
sprintf(msgBuff, Msg, __VA_ARGS__); \
Logger::getLogger().log(LoggerLevel::DEBUG, DEFTAG, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \

/*
 * Printf logging with custom tag.
 */

#define ALOGIPRINTF(Tag, Msg, ...) { \
char msgBuff[256];  \
sprintf(msgBuff, Msg, __VA_ARGS__); \
Logger::getLogger().log(LoggerLevel::INFO, Tag, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \

#define ALOGWPRINTF(Tag, Msg, ...) {\
char msgBuff[256];\
sprintf(msgBuff, Msg, __VA_ARGS__);\
Logger::getLogger().log(LoggerLevel::WARNING, Tag, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \

#define ALOGEPRINTF(Tag, Msg, ...) { \
char msgBuff[256];  \
sprintf(msgBuff, Msg, __VA_ARGS__); \
Logger::getLogger().log(LoggerLevel::ERROR, Tag, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \

#define ALOGDPRINTF(Tag, Msg, ...) { \
char msgBuff[256];  \
sprintf(msgBuff, Msg, __VA_ARGS__); \
(Logger::getLogger()).log(LoggerLevel::DEBUG, Tag, __FUNCTION__, __FILE__, __LINE__, msgBuff); \
} \



#endif // __LOG_MACROS__H__