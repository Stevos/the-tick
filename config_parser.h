#ifndef __CONFIG_PARSER_H__
#define __CONFIG_PARSER_H__

/*
 * @copyright Playffle Inc.
 * @year 2018
 *
 * Singleton config_parser component.
 *
 * Purpose is to read configuration parameters from file
 * and apply it through system without build time dependencies.
 * This is usefull to have for testing purposes and wide usage of
 * system.
 *
 * @todo Implement array
 *
 */

#include <extern_libs/json.hpp>

#include <iostream>
#include <fstream>
#include <mutex>
#include <string>


namespace playffle {

using json = nlohmann::json;

class Config_parser
{
  // Singleton stuff, disable copying
	Config_parser(const Config_parser&) = delete;
	Config_parser & operator=(const Config_parser&) = delete;

  // One to rule them all
	static std::unique_ptr<Config_parser> instance_;
	static std::once_flag once_flag_;

  // Lib handle to parse JSON
  json json_;
public:
  // Prevent default construction
	Config_parser() = delete;

  // Initialize parser from file
  Config_parser(const std::string& path) {
    std::ifstream file(path);
    file >> json_;
  }

  // Create first (and only) instance
  static void create_config(const std::string& path)
  {
    std::call_once(Config_parser::once_flag_, [path] (){
			instance_.reset(new Config_parser(path));
		});
  }

  // Get active instance
	static Config_parser& instance()
	{
		return *(instance_.get());
	}

  // Getting parameter value with given name
	template<class T>
	T get_param(const std::string& name) {
    return json_.at(name.c_str()).get<T>();
  }
};

} // end of namespace playffle

#endif //__CONFIG_PARSER_H__
