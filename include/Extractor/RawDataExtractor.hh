#ifndef __RAW_DATA_EXTRACTOR_HH__
#define __RAW_DATA_EXTRACTOR_HH__


/*
 * Intended to extract raw samples (PCM) data from various audio formats so it
 * can be processed further for feature extraction and calculation.
 *
 * It is basically wrapper around ffmpeg library which is doing heavy processing
 * related to extracting samples. Implementation code is done to be as much
 * modular as it can be. Logical parts as finding codec or processing are in
 * different method so it can be easily edited if necessary later.
 *
 * There is still influence from older design which should be refactored
 * eventually.
 */

extern "C" {
	#include "libavcodec/avcodec.h"
	#include "libavdevice/avdevice.h"
	#include "libavfilter/avfilter.h"
	#include "libavformat/avformat.h"
	#include "libavutil/avutil.h"
}


#include "Utils/Logger.hh"
#include "Data/WindowData.hh"
#include "Extractor/FFMpegResample.hh"
#include "Utils/RootException.hh"
#include "Song.h"


#include <string>

class RawDataExtractor{

	AVSampleFormat m_fmt;
	std::string m_filename;
	AVFormatContext* m_ctx;
	float m_duration; // in secs
	WindowData* m_data;
	Globals::SampleType m_sample_type;
	FFMpegResample* m_resampler;

	void extractDataInner();
	void process(AVCodecContext*);

	AVCodec* findCodec();
	void initializeFormatContext();
	void detectSampleFormat(AVSampleFormat);
	bool getFrame(AVPacket* avpkt, AVCodecContext* c, AVFrame* frame);

public:
	RawDataExtractor(std::string);
	WindowData* extractData();
	void fillMetaData(Song& song);

	~RawDataExtractor();

};

#endif // __RAW_DATA_EXTRACTOR_HH__
