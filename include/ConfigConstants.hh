#ifndef __CONFIG_CONSTANTS__H__
#define __CONFIG_CONSTANTS__H__

#include <string>

enum LoggerLevel {ERROR = 'E', WARNING = 'W', INFO = 'I', DEBUG = 'D' };

class ConfigConstants{
	friend class Logger;
public:
	static LoggerLevel conf_logLevel;
	static std::string conf_logPath;
private:
	ConfigConstants() = delete;
	ConfigConstants& operator=(const ConfigConstants&) = delete;
};

#endif //__CONFIG_CONSTANTS__H__