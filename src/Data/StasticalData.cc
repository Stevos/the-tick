//
// Created by stefan on 15.10.16..
//

#include "Data/StasticalData.hh"

#include "Globals.hh"

#include <float.h>
#include <numeric>

void StatisticalData::getCoeff(){
	float min = DBL_MAX;
	float max = DBL_MIN;
	for(auto feature : m_features){
		if(feature < min) min = feature;
		if(feature > max) max = feature;
	}

	m_coeff = 1.;

}

void StatisticalData::calculateMean() {
  float sum = std::accumulate(std::begin(m_features), std::end(m_features), 0.f);
  m_stats.push_back(sum / m_features.size() * m_coeff);
}

void StatisticalData::restOfScores() {
  float total_variance = 0;
  float total_skewness = 0;
  float total_kurtosis = 0;

  for(auto item : m_features){
		item *= m_coeff;
    total_variance += powf(item - m_features[Stats::MEAN], 2);
    total_skewness += powf(item - m_features[Stats::MEAN], 3);
    total_kurtosis += powf(item - m_features[Stats::MEAN], 4);
  }

  m_stats.push_back(total_variance / (this->m_features.size() -1 ));
  m_stats.push_back(sqrtf(m_features[Stats::VARIANCE]));
  m_stats.push_back(total_skewness / (this->m_features.size() * powf(m_features[Stats::STDDEV],3)));
  m_stats.push_back(total_kurtosis / (playffle::Config_parser::instance().get_param<uint32_t>("WindowsPerSecond")*
                                      210 * powf(m_features[Stats::STDDEV], 4)));
}

StatisticalData::StatisticalData(std::vector<float>& features)  :
  m_features(features)
{}

void StatisticalData::calculate() {
  m_stats.clear();
	this->getCoeff();
  this->calculateMean();
  this->restOfScores();
}

