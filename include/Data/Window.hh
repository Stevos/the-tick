//
// Created by stefan on 16.6.16..
//

#ifndef FFMPEG_WINDOW_HH
#define FFMPEG_WINDOW_HH

/*
 * Class for encapsulation of window data of waveform.
 *
 * NOTE: Pay attention on add method for primitive & complex types for argument passing. For int16_t is better to pass
 * argument by value, but for other cases is better to pass reference.
 *
 * NOTE: Performance can be boosted significantly by allowing class to "adopt" memory from outside.
 * */

#include <iostream>
#include <fstream>
#include <cmath>
#include <memory>

#include <Utils/Logger.hh>
#include <Globals.hh>

extern "C" {
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libavcodec/avfft.h"
}

using SampleType = int16_t;

class Window{
  static uint32_t m_win_size;
  static uint32_t m_win_size_pow;
  static uint32_t m_fft_bins;

	float* m_data;
	float* m_fft_data;
	int m_n;
	size_t m_size;

	friend class WindowFeatureExtractor;

  static void initParams();
public:
	// Constructors
	Window();
	Window(const Window& other);
	Window(Window* other);
	Window(Window&& other);

	// Operators
	Window& operator=(Window& other);
	inline float& operator[](unsigned int i){ return m_data[i];}

	inline float* fftData() { return m_fft_data; }
	inline float* data()    { return m_data; }

	// API
	inline size_t size(){return m_size;}

	// NOTE: Still need to be checked if this is correct dft. [RESEARCH]
	void fft(std::unique_ptr<float[]>& correction);

	void printToFile(std::string path);

	static void combine(Window& first, Window& second, Window& dest);

	~Window();
};

#endif //FFMPEG_WINDOW_HH
