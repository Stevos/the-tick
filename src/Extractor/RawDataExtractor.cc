#include "Extractor/RawDataExtractor.hh"

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>


extern "C" {
	#include "libavcodec/avcodec.h"
	#include "libavdevice/avdevice.h"
	#include "libavfilter/avfilter.h"
	#include "libavformat/avformat.h"
	#include "libavutil/avutil.h"
}

#undef DEFTAG
#define DEFTAG "RawExtractor"

RawDataExtractor::RawDataExtractor(std::string path)
: m_filename(path), m_ctx(nullptr){
	m_data = new WindowData();
	m_resampler = new FFMpegResample();

	ALOGD(DEFTAG, "Extractor created!");
}

RawDataExtractor::~RawDataExtractor() {
	delete m_resampler;

	avformat_close_input(&m_ctx);
	av_free(m_ctx);

}

void RawDataExtractor::detectSampleFormat(AVSampleFormat fmt){
	m_fmt = fmt;
	switch(fmt){
		case AV_SAMPLE_FMT_U8:
		case AV_SAMPLE_FMT_U8P:
			m_sample_type = Globals::SampleType::INT8;
			break;
		case AV_SAMPLE_FMT_S16:
		case AV_SAMPLE_FMT_S16P:
			m_sample_type = Globals::SampleType::INT16;
			break;
		case AV_SAMPLE_FMT_S32:
		case AV_SAMPLE_FMT_S32P:
			m_sample_type = Globals::SampleType::INT32;
			break;
		case AV_SAMPLE_FMT_FLT:
		case AV_SAMPLE_FMT_FLTP:
			m_sample_type = Globals::SampleType::FLOAT;
			break;
		case AV_SAMPLE_FMT_DBL:
		case AV_SAMPLE_FMT_DBLP:
			m_sample_type = Globals::SampleType::DOUBLE;
			break;

		default:
			m_sample_type = Globals::SampleType::ERR;
	}
}

WindowData* RawDataExtractor::extractData() {
	extractDataInner();
	return m_data;
}

void RawDataExtractor::extractDataInner(){
	AVCodec *codec;
	AVCodecContext *c = NULL;
	initializeFormatContext();
	codec = findCodec();

	if(!codec)
		throw RootException(DEFTAG, "Codec wasn't found! Exiting...");

	c = avcodec_alloc_context3(NULL);

	for (unsigned i = 0; i < m_ctx->nb_streams; ++i) {
		AVCodecParameters* tc = m_ctx->streams[i]->codecpar;
		if (tc->codec_type == AVMEDIA_TYPE_AUDIO) {
			if(avcodec_parameters_to_context(c,tc) < 0)
				throw RootException(DEFTAG, "AVCodecParameters conversion to AVCodecContext failed!");
			break;
		}
	}


	if(avcodec_open2(c, codec, 0) < 0)
		throw RootException(DEFTAG, "Error codec opening!");

	LOGDPRINTF("Sample format is: %d",(int)c->sample_fmt);

	detectSampleFormat((AVSampleFormat)c->sample_fmt);
	m_duration = (float) m_ctx->duration / AV_TIME_BASE + 1;



	LOGDPRINTF("Duration: %f", m_duration);

	process(c);

	avcodec_free_context(&c);
	avcodec_close(c);
}

AVCodec* RawDataExtractor::findCodec() {
	AVCodec* codec = m_ctx->audio_codec;
	if (codec){
		LOGD("Codec found!");
		LOGDPRINTF("Audio codec: %s (%d) ", codec->name, codec->id);
		return codec;
	}
	codec = avcodec_find_decoder(m_ctx->audio_codec_id);
	if (codec){

		LOGD("Codec found!");
		LOGDPRINTF("Audio codec: %s (%d) ", codec->name, codec->id);
		return codec;
	}
	AVOutputFormat* fmt = av_guess_format(0, m_filename.c_str(), 0);
	codec = fmt ? avcodec_find_decoder(fmt->audio_codec) : 0;

	if (codec){
		LOGD("Codec found!");
		LOGDPRINTF("Audio codec: %s (%d) ", codec->name, codec->id);
		return codec;
	}
	av_free(fmt);

	throw RootException(DEFTAG, "Unsuccessfull finding of audio codec.");
	return NULL;
}

void RawDataExtractor::initializeFormatContext(){

	m_ctx = avformat_alloc_context();

	if (avformat_open_input(&m_ctx, m_filename.c_str(), 0, 0) < 0)
		throw RootException(DEFTAG, "AVFormat opening failure! Exiting...");

	if (avformat_find_stream_info(m_ctx, 0) < 0)
		throw RootException(DEFTAG, "AVFormat finding stream info failure! Exiting...");
}

void RawDataExtractor::process(AVCodecContext *c){
	AVPacket avpkt;
	AVFrame *decoded_frame = NULL;

	decoded_frame = av_frame_alloc();
	av_init_packet(&avpkt);

	/*
	 * Here goes various settings for components which are involved in processing.
	 * Reason: Some data are only available inside of decoded frame. As number of channels
	 * or sample rate.
	 */


	while(av_read_frame(m_ctx, &avpkt) >= 0){

		if(!getFrame(&avpkt, c, decoded_frame)){
			LOGW("FrameSkipped!");
			av_frame_unref(decoded_frame);
			av_packet_unref(&avpkt);
			continue;
		}

		m_data->allocateMemory(m_duration, decoded_frame->channels);
		m_resampler->initResampler(m_fmt, decoded_frame->channel_layout, decoded_frame->sample_rate, decoded_frame->nb_samples);
		m_resampler->resampleAudioFrame(decoded_frame, m_data);

		av_frame_unref(decoded_frame);
		av_packet_unref(&avpkt);
		break;
	}

	while(av_read_frame(m_ctx, &avpkt) >= 0){

		if(!getFrame(&avpkt, c, decoded_frame)){
			LOGW("FrameSkipped!");
			av_frame_unref(decoded_frame);
			av_packet_unref(&avpkt);
			continue;
		}

		m_resampler->resampleAudioFrame(decoded_frame, m_data);

		av_frame_unref(decoded_frame);
		av_packet_unref(&avpkt);
	}

	av_frame_free(&decoded_frame);
	return;
}

bool RawDataExtractor::getFrame(AVPacket* avpkt, AVCodecContext* c, AVFrame* frame){
	// TODO: Make error messages to understand ffmpeg error codes.
	if(avcodec_send_packet(c,avpkt) < 0){

		LOGW("SendPacketError!");
		return false;
	}

	if(avcodec_receive_frame(c, frame) < 0){
		LOGW("ReceiveFrameError!");
		return false;
	}
	return true;
}

void RawDataExtractor::fillMetaData(Song& song)
{
	SongSymbolicData data;
	AVDictionaryEntry *tag = nullptr;

	if(m_ctx == nullptr)
		this->initializeFormatContext();

	while((tag = av_dict_get(m_ctx->metadata,"", tag, AV_DICT_IGNORE_SUFFIX))){
			if(strcmp(tag->key, "artist") == 0){
				data.setAlbum(tag->value);
				continue;
			}

			if(strcmp(tag->key, "name") == 0){
				data.setName(tag->value);
				continue;
			}

			if(strcmp(tag->key, "genre") == 0){
				data.setGenre(tag->value);
				continue;
			}

			if(strcmp(tag->key, "album") == 0){
				data.setAlbum(tag->value);
				continue;
			}

			if(strcmp(tag->key, "year") == 0){
				data.setYear(::atoi(tag->value));
				continue;
			}
	}

	song.setSymbolicData(data);
}










