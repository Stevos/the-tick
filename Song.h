#ifndef __SONG__H__
#define __SONG__H__

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "Distances.hh"
#include "FileSystem/FileHandle.hh"

struct SongSymbolicData{

	SongSymbolicData(){};
	SongSymbolicData(std::string artist_,
									 std::string genre_,
									 std::string name_,
									 std::string album_,
									 unsigned short year_)
		: artist(artist_), genre(genre_), name(name_), album(album_), year(year_){}

	std::string getArtist()  { return artist; }
	std::string getGenre()   { return genre; }
	std::string getName()    { return name; }
	std::string getAlbum()   { return album; }
	unsigned short getYear() { return year; }

	void setArtist(const std::string& in) { artist = in; }
	void setGenre(const std::string& in)  { genre = in; }
	void setName(const std::string& in)   { name = in; }
	void setAlbum(const std::string& in)  { album = in; }
	void setYear(const unsigned in)       { year = in; }

	void print(){
		std::cout << "========================================" << std::endl;
		std::cout << "";
		std::cout << "========================================" << std::endl;
	}

private:
	std::string artist;
	std::string genre;
	std::string name;
	std::string album;
	unsigned short year;
};

class Song {
public:
	Song(FileHandle& handle) : m_filehandle(handle), m_marked(false), m_distance(-1){}
	Song(const std::string& path) : m_filehandle(path), m_marked(false), m_distance(-1) {}
	Song() : m_filehandle(""), m_marked(false), m_distance(-1) {}

	std::string path() { return m_filehandle.getPath(); }

	// Method related to marking Song.
	void mark() { m_marked = true; }
	void unmark() { m_marked = false; }
	bool marked() { return m_marked; }

	bool processed() { return m_distance > -0.005f; }
	float distance() { return m_distance; }
	void setDistance(float dist) { m_distance = dist; }

	bool hasFeatures() { return !m_features.empty(); }
	std::vector<float>& getFeatures() { return m_features; }
	void setFeatures(const std::vector<float>& vals){ m_features = vals; }

	void changeFeatureValue(size_t index, float val) {
		m_features[index] = val;
	}

	void setSymbolicData(SongSymbolicData& data){ m_symData = data; }

private:
	SongSymbolicData m_symData;
	FileHandle m_filehandle;
	std::vector<float> m_features;

	bool m_marked;
	float m_distance;
};


#endif //__SONG__H__
