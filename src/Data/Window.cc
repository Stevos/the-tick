#include "Data/Window.hh"

#include <exception>
#include "config_parser.h"


using playffle::Config_parser;

uint32_t Window::m_win_size     = 0;
uint32_t Window::m_win_size_pow = 0;
uint32_t Window	 ::m_fft_bins   = 0;

Window::Window() :
  m_data (new float[Config_parser::instance().get_param<uint32_t>("WindowSize")]()),
  m_fft_data{nullptr},
  m_n{0},
  m_size(Config_parser::instance().get_param<uint32_t>("WindowSize"))
  {
    initParams();
  }

Window::Window(Window* other){
		throw std::logic_error("asdas");
}

Window::Window(const Window& other) {
	throw std::logic_error("Not implemented yet!");
}

Window::Window(Window&& other){
	throw std::logic_error("Not implemented yet!");
}

// Operators
// Move window.
Window& Window::operator=(Window& other) {
	throw std::logic_error("Not implemented yet!");
}

void Window::initParams()
{
  if(m_win_size == 0)
      m_win_size = Config_parser::instance().get_param<uint32_t>("WindowSize");

  if(m_win_size_pow == 0)
      m_win_size_pow = Config_parser::instance().get_param<uint32_t>("WindowSizePower");

  if(m_fft_bins == 0)
    m_fft_bins = Config_parser::instance().get_param<uint32_t>("WindowFFTBins");


}

void Window::fft(std::unique_ptr<float[]>& correction){
	m_fft_data = (float*) av_malloc(sizeof(float)*m_win_size);
	memcpy(m_fft_data, correction.get(), sizeof(float)*m_win_size);
	for(uint32_t i = 0; i < m_win_size; ++i){
			m_fft_data[i] *= m_data[i];
	}
// NOTE: Check if R2C transformation is cool here. R2R is normally used, but that doesn't exists in ffmpeg
	RDFTContext * ctx = av_rdft_init(m_win_size_pow, DFT_C2R);
	av_rdft_calc(ctx, m_fft_data);
	av_rdft_end(ctx);

// NOTE: Bashi report based guess. Check this!
	for(uint32_t i = 0; i < m_fft_bins; ++i){
		m_fft_data[i] =
			powf(m_fft_data[i],2) +
			powf(m_fft_data[m_win_size - i],2);
	}
}

void Window::printToFile(std::string path){
	std::ofstream file;
	file.open(path, std::ios::out);

	file << "Signal: ";
	for(uint32_t i = 0; i < m_win_size; ++i){
		file << m_data[i] << std::endl;
	}
	file << std::endl;

	file << "Spectral: " << std::endl;
	for(uint32_t i = 0; i < m_win_size; ++i){
		file << m_fft_data[i] << std::endl;
	}
	file << std::endl;

	file.close();
}

void Window::combine(Window& first, Window& second, Window& dest)
{
	uint32_t offset = m_win_size / 2;
	memcpy(dest.m_data, first.m_data + offset, offset * sizeof(SampleType));
	memcpy(dest.m_data + offset, second.m_data, offset * sizeof(SampleType));
}


Window::~Window() {
	if(m_data != nullptr)
		delete[] m_data;
	if(m_fft_data != nullptr)
			av_freep(&m_fft_data);
}
