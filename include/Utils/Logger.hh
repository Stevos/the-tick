#ifndef __LOGGER__H__
#define __LOGGER__H__

#include <fstream>
#include <string>
#include <ctime>
#include <cstdarg>
#include <cstring>
#include <sys/time.h>

#include "ConfigConstants.hh"
#include "LogMacros.hh"
#include "Globals.hh"

/*
 * Extensible and modular log are one of key feature of developing and supporting
 * projects. Logger is modeled to provide simple logging of different components 
 * of system. Idea is to implement simple version of logcat system on Android
 * system. 
 * 
 */

#include <cstring>

class Logger {
private:
	// Saving
	std::ofstream m_file;
	
	Logger(){
		m_file.open("/home/stefan/alderaan.log",  std::ios::out | std::ios::app );
	}
	Logger(Logger const&)          = delete;
	void operator=(Logger const&)  = delete;
	
public:
	static Logger& getLogger(){
		static Logger instance;
		return instance;
	}
	void log(LoggerLevel lvl, const char* tag, char const* function, char const* file, int line, const char* msg){
		char timeStr[32];
		timeval curTime;
		gettimeofday(&curTime, NULL);
		int milli = curTime.tv_usec / 1000;
		strftime(timeStr, 32, "%F %H:%M:%S", localtime(&curTime.tv_sec));
		
		// Stripping down whole path of file (keeping just file name and extension)
		std::string fileStr(file);
		std::string ft = std::string(fileStr.begin()+1+fileStr.rfind('/'), fileStr.end());
		
		m_file << timeStr<< "."<<milli<< " " << tag << " " ;
		
		if(LoggerLevel::ERROR == lvl)
			m_file << "\033[31m";
			
		m_file << (char)lvl << "/\033[0m " << ft << ":" << function << "#" << line << " $ " << msg << std::endl;
	}
	~Logger(){ m_file.close(); }
};


#endif // __LOGGER__H__