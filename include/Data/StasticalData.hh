//
// Created by stefan on 15.10.16..
//


#ifndef FFMPEG_STASTICALDATA_HH
#define FFMPEG_STASTICALDATA_HH

#include "Globals.hh"

#include <fstream>
#include <functional>
#include <vector>
#include <cmath>

enum Stats {
    MEAN     = 0,
    VARIANCE = 1,
    STDDEV   = 2,
    SKEWNESS = 3,
    KURTOSIS = 4
  };

class StatisticalData{
  
	float m_coeff;
  std::vector<float> m_stats;
  std::vector<float>& m_features;

	void getCoeff();
  void calculateMean();
  void restOfScores();

public: 
  StatisticalData(std::vector<float>& features);
  inline std::vector<float> getStats() { return std::move(m_stats); }
  void calculate();
  

};


#endif //FFMPEG_STASTICALDATA_HH
