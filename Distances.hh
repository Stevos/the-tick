#ifndef __DISTANCES__HH__
#define __DISTANCES__HH__

#include <vector>
#include <cmath>

#include "Utils/RootException.hh"

class IDistance {
public:
	virtual float calculate(const std::vector<float>&,
													const std::vector<float>&) = 0;
};

class EuclideanDistance : public IDistance{
public:
	float calculate(const std::vector<float>& a, const std::vector<float>& b){
		if(a.size() != b.size())
			throw DistanceException("Sizes are not same!");

		float ret = 0;
		for(size_t i = 0, size = a.size(); i < size; ++i){
			ret += powf(a[i]-b[i], 2);
		}
		return sqrtf(ret);
	}
};

#endif //__DISTANCES__HH__