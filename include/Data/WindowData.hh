#ifndef __WINDOWDATA__HH__
#define __WINDOWDATA__HH__

/*
 * Class for storing and logic for dividing samples in windows for easier
 * further developing feature extraction. Designing class with additional
 * logic to be able to easy change window size for further possible changes
 * which could be possible encountered.
 *
 *
 *
 */

#include <vector>
#include <iostream>
#include <fstream>
#include <iostream>
#include <cstdint>
#include <limits.h>
#include <memory>

#include "Globals.hh"
#include "Data/Window.hh"
#include "Utils/Logger.hh"

#undef DEFTAG
#define DEFTAG "WindowData"

class WindowData{
	uint32_t m_size;
	std::vector<Window> m_data;

  static uint32_t m_win_size;
  static uint32_t m_dest_samplerate;

	static std::unique_ptr<float[]> m_norm_win;
public:
	WindowData();

  static void initParams();

	/******* PREPARE API *******/
	// Used to allocate memory before actual data collection.
	void allocateMemory(int duration, int channels);

	inline Window& operator[](int i){ return m_data[i];}
	inline std::vector<Window>& getData(){ return this->m_data; }

	/******* USE API *******/
	// Adding Sample
	void addSample(SampleType inputSample);

	/******* POST COLLECTION API *******/
	void FFTWindows();
	inline size_t getNumberOfWindows(){ return m_data.size(); }

	/******* DEBUG API *******/
	void printToFileTestDebug(std::string pathOut);
	void logStatusDebug();

	void validate();

	void introduceHopping();

	inline bool empty() { return m_size == 0; }
};

#endif //__WINDOWDATA__HH__
