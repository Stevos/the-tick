#ifndef __PLAYLIST__H__
#define __PLAYLIST__H__

#undef DEFTAG
#define DEFTAG "RootComponent"

#include "Utils/Stopwatch.hh"
#include "Extractor/RawDataExtractor.hh"
#include "Data/WindowData.hh"
#include "Data/Window.hh"
#include "Utils/Logger.hh"
#include "Globals.hh"
#include "Data/WindowFeatureExtractor.hh"
#include "Data/StasticalData.hh"
#include <Data/feature_group.h>

#include "Song.h"

#include "FileSystem/FileHandle.hh"
#include "FileSystem/FolderHandle.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <utility>

#include "data_dumper.h"
#include "Utils/DataStore.h"
#include "Utils/RootException.hh"


class Playlist {
public:
	Playlist()
	 : m_refCheck(false)
	{
		m_datastore.checkRoutine();
	}
	~Playlist();

	void addFolder(const std::string& path,
								 int depth = 50,
								 Globals::FileFormats formats = Globals::FileFormats::MP3);

	void addSong(const std::string& path);
	void addSong(FileHandle& file);

	void listSongsDebug(){
		std::cout << "SizeOfVec : " << m_songList.size() << std::endl;
		std::cout << "SizeOfMap : " << m_sorted.size() << std::endl;
		for(auto pair : m_sorted){
			std::cout << pair.second->path() << " : " << pair.first << std::endl;
		}
	}

	void calculateDistances(){
		std::vector<float> ref;
		normalizeSongStats();
		for(auto& song : m_songList){
			playffle::DataDumper dump(song.path());
			dump.dumpWholeSetFeature(song.getFeatures());
		}

		playffle::DataDumper dumper("");
		dumper.dumpStatsMaxMin(m_songList);

		size_t curr_index = 0;
		for(size_t i = 0; i < m_songList.size(); ++i)
		{
			auto item = getNearest(m_songList[curr_index]);
			std::cout << m_songList[item.first].path() << " : " << item.second << std::endl;
			curr_index = item.first;
		}

	}

	std::pair<size_t, float> getNearest(Song& song) {
		size_t index = 0;
		float min_dist = 1000;

		std::unique_ptr<IDistance> dist(new EuclideanDistance());

		for(size_t i = 0; i < m_songList.size(); ++i) {
			if(!m_songList[i].marked()) {
				float distance = dist->calculate(song.getFeatures(), m_songList[i].getFeatures());
				if(min_dist > distance)
				{
					index = i;
					min_dist = distance;
				}
			}
		}

		m_songList[index].mark();
		return std::pair<size_t, float>(index, min_dist);
	}


	size_t getIndexForFilename(const std::string& ref) {
		for (size_t i = 0; i < m_songList.size(); ++i) {
			if(m_songList[i].path() == ref){
				return i;
			}
		}
		return m_songList.size() + 1;
	}

	void calculateDistancesFrom(size_t index) {
		Song& ref = m_songList[index];
		std::unique_ptr<IDistance> dist(new EuclideanDistance());

		for(auto& song : m_songList) {
			float distance = dist->calculate(ref.getFeatures(), song.getFeatures());
			std::cout << song.path() << " " << distance << std::endl;
		}
	}

	void addRefSong(const std::string& path);

	void normalizeSongStats(){
		std::vector<float> min = m_songList[0].getFeatures();
		std::vector<float> max = m_songList[0].getFeatures();

		for(size_t i = 1; i < m_songList.size(); ++i) {
			auto& features = m_songList[i].getFeatures();
			for(size_t j = 0; j < features.size(); ++j)
			{
				if(min[j] > features[j])
					min[j] = features[j];

				if(max[j] < features[j])
					max[j] = features[j];
			}
		}

		for(auto& song : m_songList)
		{
			auto& features = song.getFeatures();
			for(size_t i = 0; i < features.size(); ++i)
			{
				float temp = Globals::checkValue(-1 + (features[i] - min[i]) / (max[i] - min[i]) * 2);
				song.getFeatures()[i] = temp;
			}
		}
	}

private:
	void processOne(Song& song);

	DatastoreSqlite m_datastore;
	std::unique_ptr<float[]> m_norm_window;
	std::vector<Song> m_songList;

	std::vector<std::pair<Song, float>> m_list;
	std::map<float, Song*> m_sorted;

	bool m_refCheck;
	Song m_ref;
};

#endif //__PLAYLIST__H__
