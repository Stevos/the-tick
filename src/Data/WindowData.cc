#include "Data/WindowData.hh"

#include <Data/norm_window.h>

#undef DEFTAG
#define DEFTAG "WindowData"

using playffle::Config_parser;


uint32_t WindowData::m_win_size = 0;
uint32_t WindowData::m_dest_samplerate = 0;

std::unique_ptr<float[]> WindowData::m_norm_win(nullptr);

WindowData::WindowData() : m_size(0)
{
}

void WindowData::initParams() {
  auto& params = playffle::Config_parser::instance();

  if(m_win_size == 0)
    m_win_size = params.get_param<uint32_t>("WindowSize");

  if(m_dest_samplerate == 0)
    m_dest_samplerate = params.get_param<uint32_t>("DestinationSampleRate");

	if(m_norm_win.get() == nullptr)
	{
		playffle::NormalizationWindow norm_win;
		std::string win_name = playffle::Config_parser::instance().get_param<std::string>("NormFunc");
		m_norm_win = norm_win.get_window(win_name);
	}

}

void WindowData::allocateMemory(int duration, int channels){
	LOGDPRINTF("Duration: %d", duration);
	// As mentioned somewhere in code, sample rate is combined sum of samples
	// of all channels
	int temp1 = ( m_dest_samplerate / channels * (duration+1) ) / m_win_size + 1;
	m_data.resize(temp1 * 2 - 1);
}

void WindowData::addSample(SampleType inputSample) {
	float temp = Globals::checkValue(-1 + (inputSample + 32768) / (32767 *2 + 1) * 2);
	m_data[m_size/m_win_size*2][m_size%m_win_size] = temp;
	++m_size;
}

void WindowData::FFTWindows(){
	auto iter = m_data.begin();
	while(iter != m_data.end()){
		iter->fft(m_norm_win);
		++iter;
	}
}

void WindowData::printToFileTestDebug(std::string pathOut){
	std::ofstream file;
	file.open(pathOut, std::ios::out);

	std::vector<Window>::iterator iterFirst = m_data.begin();

	while(iterFirst != m_data.end()){
		for(uint32_t i = 0; i < m_win_size; ++i){
			file << (*iterFirst)[i] << std::endl;
		}
		++iterFirst;
	}
	file.close();
}

void WindowData::logStatusDebug(){
	//LOGDPRINTF("Global windows per second: %d ", Globals::WindowsPerSecond);
	//LOGDPRINTF("Global window size: %d ", Globals::WindowSize);
/*
	LOGDPRINTF("Number of sample in first window: %d ", (int) m_data.begin()->size());
	LOGDPRINTF("Number of sample in first window: %d ", (int) m_data.rbegin()->size())*/;
	LOGDPRINTF("Number of windows: %d ", (int) m_data.size());
	LOGDPRINTF("Number of samples processed: %d ", m_size);
}

void WindowData::validate()
{
	for(size_t i = 0; i < m_data.size(); ++i)
	{
		if(m_data[i].size() != m_win_size){
			LOGE("Window validation failed!");
			return;
		}
	}
	LOGI("Window validation sucessfull");
}

void WindowData::introduceHopping()
{

	LOGEPRINTF(">>>>>>>>>>>>> SIZE: %d ", m_size);
	for(uint32_t i = 1; i < m_data.size() - 1; i += 2)
	{
		Window::combine(m_data[i-1], m_data[i+1], m_data[i]);
	}
}
