#include "FileSystem/FileHandle.hh"


FileHandle::FileHandle(const fs::directory_entry& entry)
 : m_path(entry.path()){
	m_ext = getFormat(m_path);
}

FileHandle::FileHandle(const std::string& path)
	: m_path(path)
{

}


Globals::FileFormats FileHandle::getFormat(const fs::path& path)
{
	if(m_path.extension() == ".mp3"){
		return Globals::FileFormats::MP3;
	}
	if(m_path.extension() == ".wma"){
		return Globals::FileFormats::WMA;
	}
	if(m_path.extension() == ".wmv"){
		return Globals::FileFormats::WMV;
	}
	if(m_path.extension() == ".ogg"){
		return Globals::FileFormats::OGG;
	}
	if(m_path.extension() == ".aac"){
		return Globals::FileFormats::AAC;
	}
	if(m_path.extension() == ".flac"){
		return Globals::FileFormats::FLAC;
	}

	// Not possible to happen! But still its good practise to leave
	// error codes and error handling option hanging.
	return Globals::FileFormats::ERR;
}


std::string FileHandle::toString()
{
	return m_path;
}


FileHandle::~FileHandle(){}

