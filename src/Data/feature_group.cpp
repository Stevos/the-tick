#include <Data/feature_group.h>

#include <Globals.hh>

namespace playffle {

	FeatureGroup::FeatureGroup(const VVFloat& in) : m_data(in)
	{
		initParams();
	}

	VVFloat FeatureGroup::getFeatureGroups()
	{
		VVFloat ret;
		uint32_t mark = 0;
		while((mark + m_win_per_group) < m_data.size())
		{
			ret.emplace_back(getFeatureBlock(mark, mark + m_win_per_group));
			mark += m_win_per_group;
		}

		if( (m_data.size() - mark) > 5)
		{
			ret.emplace_back(getFeatureBlock(mark, m_data.size() - 1));
		}

		return ret;
	}

	std::vector<float> FeatureGroup::getFeatureBlock(uint32_t start, uint32_t end)
	{
		std::vector<float> ret(WindowFeatureExtractor::WINDOW_FEATURE_SIZE * 4);

		for(uint16_t i = 0; i < WindowFeatureExtractor::WINDOW_FEATURE_SIZE; ++i) {
			ret[i * 4]     =  f_mean(start,
															 end,
														   static_cast<WindowFeatureExtractor::WindowFeatures>(i));
			ret[i * 4 + 1] =  f_variance(start,
																	 end,
																   static_cast<WindowFeatureExtractor::WindowFeatures>(i),
																   ret[i*4]);

			float std_dev = sqrt(ret[i*4 + 1]);

			ret[i * 4 + 2] =  f_skewness(start,
																	 end,
																	 static_cast<WindowFeatureExtractor::WindowFeatures>(i),
																   ret[i*4],
																   std_dev);

			ret[i * 4 + 3] =  f_kurtosis(start,
																	 end,
																   static_cast<WindowFeatureExtractor::WindowFeatures>(i),
																   ret[i*4],
																   std_dev);
		}

		return ret;
	}
	void FeatureGroup::initParams()
	{
		m_win_per_group = Config_parser::instance().get_param<uint32_t>("WindowsPerBlock");
	}

	float FeatureGroup::f_mean(uint32_t start,
														 uint32_t end,
														 WindowFeatureExtractor::WindowFeatures feature)
	{
		float sum = 0.f;
		for(uint32_t i = start; i < end; ++i) {
			sum += m_data[i][feature];
		}

		return Globals::checkValue(sum / (end - start));
	}

	float FeatureGroup::f_variance(uint32_t start,
																 uint32_t end,
																 WindowFeatureExtractor::WindowFeatures feature,
																 float mean)
	{
		float sum = 0.f;
		for(uint32_t i = start; i < end; ++i)
		{
			sum += powf(m_data[i][feature] - mean, 2);
		}
		return Globals::checkValue(sum / (end - start - 1));
	}

	float FeatureGroup::f_kurtosis(uint32_t start,
																 uint32_t end,
																 WindowFeatureExtractor::WindowFeatures feature,
																 float mean,
																 float std_dev)
	{
		float sum = 0.f;
		for(uint32_t i = start; i < end; ++i)
		{
			sum += powf(m_data[i][feature] - mean, 4);
		}

		return Globals::checkValue(sum / ((end - start) * pow(std_dev, 4)));
	}

	float FeatureGroup::f_skewness(uint32_t start,
																 uint32_t end,
																 WindowFeatureExtractor::WindowFeatures feature,
																 float mean,
																 float std_dev)
	{
		float sum = 0.f;
		for(uint32_t i = start; i < end; ++i)
		{
			sum += powf(m_data[i][feature] - mean, 3);
		}

		return Globals::checkValue(sum / ((end - start) * pow(std_dev, 3)));
	}

	void FeatureGroup::setWindowPerBlock(uint32_t input)
	{
		m_win_per_group = input;
	}

	std::vector<float> FeatureGroup::getFeaturesForWholeSong()
	{
		return getFeatureBlock(0, m_data.size()-1);
	}

} // namespace end playffle::
