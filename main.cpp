#include <iostream>
#include <string>
#include <vector>


extern "C" {
	#include "libavcodec/avcodec.h"
	#include "libavdevice/avdevice.h"
	#include "libavfilter/avfilter.h"
	#include "libavformat/avformat.h"
	#include "libavutil/avutil.h"
}
#include "Data/Window.hh"
#include <Data/WindowData.hh>
#include "Utils/Logger.hh"

#include "Playlist.h"
#include "Parser.h"
#include "FileSystem/FolderHandle.h"

#include "config_parser.h"
using playffle::Config_parser;
std::unique_ptr<Config_parser> Config_parser::instance_;
std::once_flag Config_parser::once_flag_;

#include <Data/norm_window.h>

#undef DEFTAG



int main(int argc, char **argv) {

  Config_parser::create_config("config.json");
  ALOGI("START!","------------------------------");
  av_register_all();

  Stopwatch clock;
  clock.start();

	WindowData::initParams();


  Playlist list;
	list.addFolder(Config_parser::instance().get_param<std::string>("InputDirectory"));
  clock.end();

	if(Config_parser::instance().get_param<bool>("SingleCycle"))
	{
		list.normalizeSongStats();
		std::string chosen = Config_parser::instance().get_param<std::string>("SingleCycleRefSong");
		std::cout << "Chosen: " << chosen << std::endl;
		size_t index = list.getIndexForFilename(chosen);
		list.calculateDistancesFrom(index);
	}
	else {
		list.calculateDistances();
		list.listSongsDebug();
	}
  std::cout << "Time needed: " << clock.elapsed()/1000 << "s" << std::endl;
  ALOGI("END!","------------------------------");

  std::cout << "end" << std::endl;
  return 0;
}
