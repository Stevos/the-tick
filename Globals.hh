#ifndef __GLOBALS_HH__
#define __GLOBALS_HH__

/*****************************************************************************/

/*
 * Global space for values needed for whole project. Later can be used to easy
 * distribute settings from one place.
 */

#include <string>
#include <cmath>

#include "config_parser.h"

class Globals {
public:
	enum class OS { LINUX = 0,
									WINDOWS = 1,
									ANDROID = 2,
									MACOS = 3,
									IOS = 4
	};

	enum class FileFormats {
													 ERR = -1,
													 MP3 = 1,
													 WMV = 2,
													 WMA = 4,
													 AAC = 8,
													 FLAC = 16,
													 OGG = 32
	};

	enum class SampleType {  ERR = -1,
													 INT8 = 0,
													 INT16 = 1,
													 INT32 = 2,
													 DOUBLE = 3,
													 FLOAT = 4
	};

	static float checkValue(float value){
		if(std::isnan(value)) return 0.f;
		if(std::isinf(value) && value < 0.f) return -1.f;
		if(std::isinf(value)) return 1.f;
		return value;
	}
};



#endif //__GLOBALS_HH__
